<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
    
        <title>Quizz</title>
    
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
        <!--<link rel="stylesheet" href="style.css">-->
 
    </head>
 
    <body>

    <?php

        require_once('quizz.class.php');
        require_once('question.class.php');

        $quizzVache = new Quizz ("Quizz vache", 
            array (
                new Question ("Première question", "cloud", "radio", "De quelle couleur est un nuage ?", "white", 1,
                    array (
                        new Choix("Bleu", "blue_cloud", "blue"),
                        new Choix("Blanc", "white_cloud", "white"),
                        new Choix("Rouge", "red_cloud", "red")
                    )
                ),
                new Question ("Deuxième question", "snow", "radio", "De quelle couleur est la neige ?", "white", 1,
                    array (
                        new Choix("Blanc", "white_snow", "white"),
                        new Choix("Rose", "pink_snow", "pink"),
                        new Choix("Jaune", "yellow_snow", "yellow")
                    )
                ),
                new Question ("Troisième question", "milk", "radio", "De quelle couleur est le lait ?", "white", 1,
                    array (
                        new Choix("Blanc", "white_milk", "white"),
                        new Choix("Violet", "purple_milk", "purple"),
                        new Choix("Marron", "brown_milk", "brown")
                    )
                ),
                new Question ("Quatrième question", "drink", "text", "Que boit la vache ?", "eau", 2, "drink")
            )
        );
        foreach ($quizzVache->getQuestions() as $q) $q->setTheQuizz($quizzVache);

        
    ?>

 
        <?php if ( !isset($_GET) || empty($_GET) ) : ?>
 
           <h3><?=$quizzVache->getTitle()?></h3><hr><br/>
 
           <form action="" method="GET">

                <?php foreach ($quizzVache->getQuestions() as $question): ?>
                        <?=$question->toString()?></br>
                <?php endforeach; ?>

                </br>
               <button type="submit" class="btn btn-primary">Valider</button>
 
           </form>

        <?php else: ?>

            <?php
                foreach ($quizzVache->getQuestions() as $question) {
                    $question->answer($_GET[$question->getName()]);
                }
            ?>

            <p>
                Réponses correctes: <?=$quizzVache->getQuestionScore()?> / <?=count($quizzVache->getQuestions())?><br/>
                Votre score: <?=$quizzVache->getScore()?> / <?=$quizzVache->scoreMax()?>
            </p>
 
       <?php endif; ?>
 
   </body>
 
</html>