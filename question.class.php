<?php

    require_once('choix.class.php');

    class Question {

        private $title, $name, $type, $text, $answer, $score, $choices, $id, $theQuizz;

        public function __construct($aTitle, $aName, $aType, $aText, $anAnswer, $aScore, $choicesListOrId) {
            $this->title = $aTitle;
            $this->name = $aName;
            $this->type = $aType;
            $this->text = $aText;
            $this->answer = $anAnswer;
            $this->score = $aScore;
            if (is_string($choicesListOrId)) {
                $this->choices = NULL;
                $this->id = $choicesListOrId;
            } else {
                $this->choices = $choicesListOrId;
                $this->id = NULL;
            }
        }

        public function getTitle() {return $this->title;}
        public function getName() {return $this->name;}
        public function getType() {return $this->type;}
        public function getText() {return $this->text;}
        public function getAnswer() {return $this->answer;}
        public function getScore() {return $this->score;}
        public function getChoices() {if(!empty($this->title)) return $this->title;}
        public function getId() {if(!empty($this->id)) return $this->id;}
        public function getTheQuizz() {if(!empty($this->theQuizz)) return $this->theQuizz;}

        public function setTheQuizz($aQuizz) {$this->theQuizz = $aQuizz;}


        public function toString () {

            switch($this->type) {

                case 'text':
                    $html = "
                        <h6>$this->title</h6>
                        <label class='form-label' for='$this->id'>$this->text</label>
                        <input type='text' id='$this->id' name='$this->name'>
                    ";
                    break;

                case 'radio':
                    $html = "
                        <h6>$this->title</h6>
                        <label class='form-label'>$this->text</label>
                    ";
                    foreach ($this->choices as $choice) {
                        $html.= $choice->toString($this->name);
                    }
                    break;

            }

            return $html;

        }


        public function answer ($theAnswer) {

            switch($this->type) {

                case 'text': case 'radio':
                    if (!empty($theAnswer)) {
                        if (strtolower($this->answer) == $theAnswer) {
                            $this->getTheQuizz()->setQuestionScore($this->getTheQuizz()->getQuestionScore()+1);
                            $this->getTheQuizz()->setScore($this->getTheQuizz()->getScore()+$this->getScore());
                        }
                    }
                    break;

            }

        }


    }