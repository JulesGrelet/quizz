Projet déposé sur git : https://gitlab.com/JulesGrelet/quizz.git

Fonctionnement du quizz

index.php : point d'entrée du projet
ajout de bootstrap5 l8
ajout des classes Quizz et Question l17
nouvelle instance de Quizz ($quizzVache) avec une liste d'instances de Question (les questions à choix
multiples possèdent une liste d'instances de Choix) l20

si pas de données en GET : affichage du formulaire l52,
sinon : affichage du score l67

formulaire :
titre du quizz
form method GET
pour chaque question du quizz : appel de son toString() : renvoi l'html de la question
boutton submit de validation

score :
pour chaque question du quizz : appel de la méthode answer(),
paramètre donnée GET du input.name de la question
affichage du score avec les fonctions getQuestionScore(), getScore() et scoreMax() du quizz


quizz.class.php : classe Quizz
attributs $title, $questions, $questionScore=0, $score=0
constructeur titre, questions (liste vide ou renseignée)
getteur des attributs
setteur de $questionScore et $scoreMax
méthode addQuestion() ajoute une instance de Question à la liste $questions
fonction scoreMax() retourne le score maximum possible du quizz


question.class.php : classe Question
ajout de la classe Choix
attributs $title, $name, $type, $text, $answer, $score, $choices, $id, $theQuizz
constructeur titre, name, type, texte, reponse, score, liste de choix ou id (l'un est attribué l'autre null)
getteur des attributs et de $theQuizz
setteur de $theQuizz
fonction toString() selon le type de la qestion, retourne l'html de la question avec les input et labels
méthode answer() selon le type de la question, et selon la réponse en GET, met à jour le score


choix.class.php : classe Choix
attributs $text, $id, $value
constructeur texte, id, valeur
getteur des attributs
fonction toString() retourne l'html d'un choix d'une question multichoix avec les input et labels


réponse aux questions du quizz vache pour tester :
1 Blanc
2 Blanc
3 Blanc
4 eau