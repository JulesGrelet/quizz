<?php

    class Choix {

        private $text, $id, $value;

        public function __construct($aText, $anId, $aValue) {
            $this->text = $aText;
            $this->id = $anId;
            $this->value = $aValue;
        }

        public function getText() {return $this->text;}
        public function getId() {return $this->id;}
        public function getValue() {return $this->value;}


        public function toString ($className) {
            return "
                <div class='form-check'>
                    <input type='radio' class='form-check-input' id='$this->id' name='$className' value='$this->value'>
                    <label for='$this->id' class='form-check-label'>$this->text</label>
                </div>
            ";
        }


    }