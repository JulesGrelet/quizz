<?php

    class Quizz {

        private $title, $questions, $questionScore, $score;

        public function __construct($aTitle, $questionsList=array()) {
            $this->title = $aTitle;
            $this->questions = $questionsList;
            $this->setQuestionScore(0);
            $this->setScore(0);
        }

        public function getTitle() {return $this->title;}
        public function getQuestions() {return $this->questions;}
        public function getQuestionScore() {return $this->questionScore;}
        public function getScore() {return $this->score;}

        public function setQuestionScore($aQuestionScore) {$this->questionScore=$aQuestionScore;}
        public function setScore($aScore) {$this->score=$aScore;}


        public function addQuestion ($question) {
            array_push($this->questions, $question);
        }

        public function scoreMax() {
            $scoreMax = 0;
            foreach ($this->questions as $question)
                $scoreMax+= $question->getScore();
                
            return $scoreMax;
        }

    }